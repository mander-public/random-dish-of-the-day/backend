from django.urls import path
from rest_auth.views import (
    LogoutView,
    UserDetailsView,
    PasswordChangeView,
    PasswordResetView,
)

from apps.authentication.views.password_reset_confirmation import PasswordResetConfirmationView
from apps.authentication.views.user_login import UserLoginView

auth_urls = [
    # URLs that do not require a session or valid token
    path("v1/password/reset/", PasswordResetView.as_view(), name="rest_password_reset"),
    path(
        "v1/password/reset/confirm/",
        PasswordResetConfirmationView.as_view(),
        name="rest_password_reset_confirm",
    ),
    # path(
    #     "v2/login/confirm/",
    #     AuthenticationDoubleFactorAPIView.as_view(),
    #     name="login_confirm",
    # ),
    path("v1/login/", UserLoginView.as_view(), name="rest_login"),
    # URLs that require a user to be logged in with a valid session / token.
    path("v1/logout/", LogoutView.as_view(), name="rest_logout"),
    path("v1/user/", UserDetailsView.as_view(), name="rest_user_details"),
    path(
        "v1/password/change/", PasswordChangeView.as_view(), name="rest_password_change"
    ),
]

urlpatterns = auth_urls