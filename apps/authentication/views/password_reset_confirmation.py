from django.utils.translation import pgettext_lazy
from rest_auth.views import PasswordResetConfirmView
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response


class PasswordResetConfirmationView(PasswordResetConfirmView):
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        # Regardless of the exception, a Validation error - Invalid password will be returned
        try:
            serializer.is_valid(raise_exception=True)
        except ValidationError:
            raise ValidationError(
                {
                    "non_field_errors": [
                        pgettext_lazy("Validation error", "Invalid password")
                    ]
                }
            )

        serializer.save()

        return Response(
            {
                "detail": pgettext_lazy(
                    "Password reset", "Password has been reset with the new password."
                )
            }
        )

