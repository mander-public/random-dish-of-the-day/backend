# -*- coding: utf-8 -*-
import logging

from django.contrib.auth import user_logged_in
from rest_auth.views import LoginView
from rest_framework import status
from rest_framework.exceptions import ValidationError

logger = logging.getLogger("django")


class UserLoginView(LoginView):
    """
        Overriding default LoginView from rest-auth to complete login path with Entity verification.
    """

    def login(self):
        """
            Override of login method so we can save the model and update last_login on user. Later we can use this
            to block user tokens.
        :return:
        """
        super().login()
        user_logged_in.send(
            sender=self.user.__class__, request=self.request, user=self.user
        )

    # def post(self, request, *args, **kwargs):
    #     username = request.data.get("username", "")
    #     cache_key = self.make_login_cache_key(username)
    #
    #     try:
    #         self.serializer = self.get_serializer(data=self.request.data, context={'request': request})
    #         self.serializer.is_valid(raise_exception=True)
    #         super().login()
    #         self.check_password_expired()
    #         response = self.get_response()
    #
    #     except ValidationError as e:
    #         self.log_login(
    #             logging.WARNING,
    #             username,
    #             "Invalid credentials",
    #             request,
    #             status.HTTP_400_BAD_REQUEST,
    #         )
    #         self.register_failed_attemps_login(cache_key, request, username)
    #
    #         raise e
    #
    #     except Exception as e:
    #         self.log_login(
    #             logging.ERROR,
    #             username,
    #             str(e),
    #             request,
    #             status.HTTP_500_INTERNAL_SERVER_ERROR,
    #         )
    #         raise e
    #
    #     self.log_success_response(cache_key, request)
    #     return response

    # def get_response(self):
    #     self.check_password_expired()
    #
    #     employee = Employee.objects.get(user=self.user, entity=self.request.entity)
    #     pin = Pin.objects.get_authentication_double_factor(employee.uuid)
    #
    #     delay_task_celery(
    #         send_email_double_factor_task,
    #         employee_id=employee.pk,
    #         language=employee.language,
    #     )
    #     return Response(
    #         data={"owner": str(employee.uuid), "resource": str(pin.resource)},
    #         status=status.HTTP_200_OK,
    #     )

    # def check_password_expired(self):
    #     passwords_filter = self.user.history_passwords.order_by("-created").first()
    #     if passwords_filter and passwords_filter.created < now() - timedelta(
    #         days=settings.DAYS_TO_EXPIRE
    #     ):
    #         send_expired_password(
    #             get_object_or_404(
    #                 Employee.objects.filter(entity=self.request.entity), user=self.user
    #             )
    #         )
    #
    #         raise ValidationError({"non_field_errors": ["password_expired"]})

    # def log_success_response(self, cache_key, request):
    #     self.log_login(
    #         "INFO", self.user.email, "Login success", request, status.HTTP_200_OK
    #     )
    #     cache.delete(cache_key)
    #
    # def register_failed_attemps_login(self, cache_key, request, username):
    #     user_login_tries = cache.get(cache_key, 0) + 1
    #     cache.set(cache_key, user_login_tries, timeout=settings.LOGIN_CACHE_TIMEOUT)
    #
    #     # We don't want to saturate the database with block registries so we check if user is blocked already
    #     if (
    #         user_login_tries >= settings.LOGIN_ATTEMPTS
    #         and not UserRegistry.objects.is_username_blocked(username)
    #     ):
    #         try:
    #             attempt_user = User.objects.get(username=username)
    #             UserRegistry.objects.create(
    #                 user=attempt_user, ip=get_user_ip(request), reason=BLOCKED
    #             )
    #             delay_task_celery(
    #                 send_blocked_email_user,
    #                 user_id=attempt_user.id,
    #                 entity_id=request.entity.id,
    #                 language=request.LANGUAGE_CODE,
    #             )
    #         except User.DoesNotExist:
    #             pass
    #
    # @staticmethod
    # def log_login(level, username, msg, request, status_code):
    #     log_message(
    #         level,
    #         "[{}] [{}] [{}] [{}] [{}]".format(
    #             request.domain_origin, username, get_user_ip(request), status_code, msg
    #         ),
    #     )

    @staticmethod
    def make_login_cache_key(username):
        return f"login_{username}"
