from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from rest_framework.filters import SearchFilter
from django_filters.rest_framework import DjangoFilterBackend

from apps.ingredient.models.ingredient import Ingredient
from apps.ingredient.serializers.ingredient import IngredientSerializer


class IngredientViewSet(ModelViewSet):
    serializer_class = IngredientSerializer
    queryset = None
    permission_classes = [IsAuthenticated]
    lookup_field = "uuid"
    filter_backends = [DjangoFilterBackend, SearchFilter]

    def get_serializer_context(self):
        context = super(IngredientViewSet, self).get_serializer_context()
        context.update({"user": self.request.user})
        return context

    def get_queryset(self):
        self.search_fields = ("name", )
        return Ingredient.objects.filter(owner_id=self.request.user.id).order_by('name')
