from rest_framework import serializers

from apps.ingredient.models.ingredient import Ingredient


class IngredientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredient
        fields = (
            "id",
            "name",
            "color",
            "uuid"
        )

        read_only_fields = ("id", )

    def create(self, validated_data):
        validated_data["owner"] = self.context.get("user")
        self.instance = Ingredient.objects.create(
            **validated_data
        )
        return self.instance
