from rest_framework.routers import SimpleRouter

from apps.ingredient.views.ingredient import IngredientViewSet

router = SimpleRouter()
router.register(
    r"v1/ingredient",
    IngredientViewSet,
    basename="ingredient"
)

urlpatterns = router.urls
