from django.contrib import admin

from apps.ingredient.models.ingredient import Ingredient


@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    """This is a comment"""

    list_display = ("id", "name", "color", "created", "modified", "uuid")

