from django.db import models

from main.models import Timestampable, Uuidable, Nameable, Descriptable


class Ingredient(Timestampable, Uuidable, Nameable):
    color = models.CharField(max_length=8)
    owner = models.ForeignKey("auth.User", on_delete=models.CASCADE, related_name="ingredients")

    def __str__(self):
        return "{} - {}".format(self.name, self.owner.email)