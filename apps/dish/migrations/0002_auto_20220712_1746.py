# Generated by Django 3.2.6 on 2022-07-12 17:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dish', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='dish',
            name='calories',
            field=models.IntegerField(blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='dish',
            name='recipe',
            field=models.TextField(blank=True, default=None, null=True),
        ),
    ]
