from django.contrib import admin

from apps.dish.models.ingredient_dish import IngredientDish


@admin.register(IngredientDish)
class IngredientDishAdmin(admin.ModelAdmin):
    """This is a comment"""

    list_display = ("id", "ingredient", "dish", "created", "modified")
