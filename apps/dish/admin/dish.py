from django.contrib import admin

from apps.dish.models.dish import Dish


@admin.register(Dish)
class DishAdmin(admin.ModelAdmin):
    """This is a comment"""

    list_display = ("id", "name", "owner", "created", "modified", "uuid")