from rest_framework.routers import SimpleRouter

from apps.dish.views.dish import DishViewSet

router = SimpleRouter()
router.register(
    r"v1/dish",
    DishViewSet,
    basename="dish"
)

urlpatterns = router.urls
