from django.db import models

from main.models import Timestampable


class IngredientDish(Timestampable):
    ingredient = models.ForeignKey("ingredient.Ingredient", on_delete=models.CASCADE, related_name="dishes")
    dish = models.ForeignKey("dish.Dish", on_delete=models.CASCADE, related_name="ingredients")
