from django.db import models

from apps.ingredient.models import Ingredient
from main.models import Timestampable, Uuidable, Nameable, Descriptable


class Dish(Timestampable, Uuidable, Nameable, Descriptable):
    owner = models.ForeignKey("auth.User", on_delete=models.CASCADE, related_name="dishes")
    calories = models.IntegerField(null=True, blank=True, default=None)
    recipe = models.TextField(null=True, blank=True, default=None)

    def __str__(self):
        return "{} - {}".format(self.name, self.owner.email)

    def create_main_ingredients(self, ingredients):
        from apps.dish.models import IngredientDish
        from apps.ingredient.models import Ingredient

        for ingredient in ingredients:
            ingredient = Ingredient.objects.get(uuid=ingredient)
            IngredientDish.objects.create(ingredient=ingredient, dish=self)

    def import_to_other_user(self, user):
        new_dish = Dish(
            name=self.name,
            description=self.description,
            owner=user,
            calories=self.calories,
            recipe=self.recipe,
        )

        new_dish.save()
        from apps.dish.models import IngredientDish
        for ingredient_dish in self.ingredients.all():
            ingredient = ingredient_dish.ingredient
            new_ingredient, _ = Ingredient.objects.get_or_create(
                name=ingredient.name,
                owner=user,
                defaults={
                    "color": ingredient.color
                }
            )
            IngredientDish.objects.create(ingredient=new_ingredient, dish=new_dish)
