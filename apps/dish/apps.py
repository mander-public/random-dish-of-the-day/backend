from django.apps import AppConfig


class DishConfig(AppConfig):
    name = "apps.dish"
