from rest_framework import serializers

from apps.dish.models.dish import Dish
from apps.dish.serializers.ingredient_dish import IngredientDishSerializer
from apps.ingredient.serializers.ingredient import IngredientSerializer


class DishSerializer(serializers.ModelSerializer):
    ingredients = IngredientDishSerializer(many=True)

    class Meta:
        model = Dish
        fields = (
            "id",
            "name",
            "description",
            "created",
            "modified",
            "calories",
            "recipe",
            "ingredients",
            "uuid"
        )

        read_only_fields = ("id", )


class CreateDishSerializer(serializers.ModelSerializer):
    ingredients = serializers.ListField(child=IngredientSerializer(), write_only=True)

    class Meta:
        model = Dish
        fields = (
            "id",
            "name",
            "description",
            "created",
            "modified",
            "calories",
            "recipe",
            "ingredients",
            "uuid"
        )

        read_only_fields = ("id",)

    def create(self, validated_data):
        ingredients = validated_data.pop("ingredients", [])
        ingredients = list(map(lambda ingredient: str(ingredient.get("uuid")), ingredients))

        validated_data["owner"] = self.context.get("user")
        self.instance = Dish.objects.create(
            **validated_data
        )
        self.instance.create_main_ingredients(ingredients)
        return self.instance
