from rest_framework import serializers

from apps.dish.models import IngredientDish
from apps.ingredient.serializers.ingredient import IngredientSerializer


class IngredientDishSerializer(serializers.ModelSerializer):
    ingredient = IngredientSerializer()

    class Meta:
        model = IngredientDish
        fields = ("ingredient", )

    def to_representation(self, instance):
        response = super().to_representation(instance)
        return response.get("ingredient", {})
