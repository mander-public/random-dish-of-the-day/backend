from rest_framework import status
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend
from django.shortcuts import get_object_or_404

from apps.dish.models.dish import Dish
from apps.dish.serializers.dish import DishSerializer, CreateDishSerializer


class DishViewSet(ModelViewSet):
    serializer_class = DishSerializer
    queryset = None
    permission_classes = [IsAuthenticated]
    lookup_field = "uuid"
    filter_backends = [DjangoFilterBackend, SearchFilter]

    def get_serializer_context(self):
        context = super(DishViewSet, self).get_serializer_context()
        context.update({"user": self.request.user})
        return context

    def get_queryset(self):
        self.search_fields = ("name", "description", "calories", "uuid")
        if self.action == "import_dish":
            return Dish.objects.exclude(owner_id=self.request.user.id).order_by('name')
        else:
            return Dish.objects.filter(owner_id=self.request.user.id).order_by('name')

    def get_serializer_class(self):
        if self.action == "create":
            self.serializer_class = CreateDishSerializer
        return self.serializer_class

    @action(detail=False, methods=["post"], url_path="import")
    def import_dish(self, request, *args, **kwargs):
        uuid = self.request.data.get("uuid")
        dish = get_object_or_404(self.get_queryset(), uuid=uuid)
        user = self.request.user
        dish.import_to_other_user(user)
        return Response(status=status.HTTP_201_CREATED)
