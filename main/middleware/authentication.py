from django.conf import settings
from django.http import HttpResponseForbidden
from rest_framework_jwt.utils import jwt_decode_handler


class OneSessionPerUser:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if settings.ENABLE_ONE_SESSION_MIDDLEWARE:
            try:
                if request.auth:
                    org_iat = jwt_decode_handler(request.auth).get("orig_iat")
                    last_login = int(request.user.last_login.timestamp())
                    if org_iat != last_login:
                        raise ValueError("Token blocked")
            except AttributeError:
                pass
            except ValueError:
                return HttpResponseForbidden()
        return response
