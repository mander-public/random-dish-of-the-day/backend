# -*- coding: utf-8 -*-

from django.db import models


class Descriptable(models.Model):
    description = models.TextField(null=True, blank=True)

    class Meta:
        abstract = True
