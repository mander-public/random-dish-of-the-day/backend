# -*- coding: utf-8 -*-

from .timestable import Timestampable
from .nameable import Nameable
from .descriptable import Descriptable
from .uuidable import Uuidable

__all__ = [
    "Timestampable",
    "Nameable",
    "Descriptable",
    "Uuidable",
]
